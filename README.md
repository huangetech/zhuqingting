# 竹蜻蜓 - 聚合搜索平台

#### 项目介绍
整合了各类常用的搜索引擎，用户只需要选择相应的网站即可一键直达。

完全自定义修改或添加内容，做最有特色的导航网站。


#### 效果截图

![输入图片说明](https://images.gitee.com/uploads/images/2018/1017/115549_6877e18c_2236313.gif "21.gif")


#### 项目演示

http://demo.hezi.moqingwu.com/zhuqingting/


#### 参与贡献

1. 使用layui框架
2. 回首间、念念不忘


#### 灵感来源

1. 帝王组站长工具、搜站


#### 更多源码

http://hezi.moqingwu.com
