/*
  Demo1.js
  使用Layui的form和upload模块
  */
  layui.use(['jquery', 'layer', 'element', 'util', 'table','form','carousel','flow'], function() {
    var $ = layui.$,
    layer = layui.layer,
    element = layui.element,
    util = layui.util,
    table = layui.table,
    form = layui.form;
    carousel = layui.carousel;
    var flow = layui.flow;
    //页面侧边栏默认为展开时
    //
    //页面side-switch图标需设置为f-dedent
    //页面side中的菜单nav-item-a需要去掉nav-item-b
    //页面还有就是layui-body和layui-header及layui-layout-left需要去掉layui-body-50
    //执行下面代码可完美切换侧边栏状态
    //
    //点击side-switch切换左侧导航
    $('.side-switch').on('click',function(){
      //切换图标状态
      $('.side-switch i').toggleClass("fa-dedent");
      $('.side-switch i').toggleClass("fa-indent");
      $('.layui-body').toggleClass("layui-body-50");
      $('.layui-layout-left').toggleClass("layui-body-50");
      $('.side').toggleClass("side-sm");
      $('.layui-nav-item').removeClass("layui-nav-itemed");
      $('.nav-item-a').toggleClass("nav-item-b");
      //点击下拉菜单，展开侧边栏
      $('.nav-item-b').on('click',function(){
        $('.side').removeClass("side-sm");
        $('.layui-body').removeClass("layui-body-50");
        $('.side-switch i').toggleClass("fa-dedent");
        $('.side-switch i').toggleClass("fa-indent");
      });
      $('.layui-nav-tree .layui-nav-item').on('click',function(){
        $('.layui-layout-left').removeClass("layui-body-50");
        $('.nav-item-a').removeClass("nav-item-b");
        $('.side-switch i').addClass("fa-dedent");
        $('.side-switch i').removeClass("fa-indent");
      }); 
    });

    
    //移动端侧边栏展开收起状态
    $('.side-btn').on('click',function(){
      //切换图标状态
      $("body").toggleClass("showMenu");
      $('.nav-item-b').on('click',function(){
        $('.side').removeClass("side-sm");
        $('.layui-body').removeClass("layui-body-50");
      });
      $(".layui-body").toggleClass("layui-body-200");
      $('.mask').on('click',function(){
        $('body').removeClass('showMenu modal-open');
      }); 
    });

    //搜索
    $('.souchangyong').on('click',function(){
      //切换图标状态
      $(".t-s").addClass("layui-hide");
      $(".listchangyong").removeClass("layui-hide");
    });
    $('.soutool').on('click',function(){
      //切换图标状态
      $(".t-s").addClass("layui-hide");
      $(".listtool").removeClass("layui-hide");
    });
    $('.soushequ').on('click',function(){
      //切换图标状态
      $(".t-s").addClass("layui-hide");
      $(".listshequ").removeClass("layui-hide");
    });
    $('.souimg').on('click',function(){
      //切换图标状态
      $(".t-s").addClass("layui-hide");
      $(".listimg").removeClass("layui-hide");
    });
    $('.souwork').on('click',function(){
      //切换图标状态
      $(".t-s").addClass("layui-hide");
      $(".listwork").removeClass("layui-hide");
    });
    $('.sousou').on('click',function(){
      //切换图标状态
      $(".t-s").addClass("layui-hide");
      $(".listsousou").removeClass("layui-hide");
    });
    $('.login-btn').on('click',function (event) {
        event.stopPropagation();//阻止事件冒泡
        $(this).siblings('.login-box').toggle();
        //点击空白处，下拉框隐藏-------开始
        var tag = $(this).siblings('.login-box');
        var flag = true;
        $(document).bind("click",function(e){//点击空白处，设置的弹框消失
            var target = $(e.target);
            if(target.closest(tag).length == 0 && flag == true){
                $(tag).hide();
                flag = false;
            }
        });
        //点击空白处，下拉框隐藏-------结束
    });

})